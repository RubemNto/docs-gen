#include <iostream>
#include <fstream>
#include <string>
#include <filesystem>
#include <nlohmann/json.hpp>
#include "../include/parser.h"

int main(int argc, char** argv) {
  const char* config_path = "docsgen.json";
  std::fstream stream(config_path);

  if (stream.is_open())
  {
    nlohmann::json json_data = nlohmann::json::parse(stream);
    std::filesystem::create_directory(json_data["project_output_directory"]);
    std::string out_path = json_data["project_output_directory"];
    
    for(const auto path : json_data["source_directories"]){
      for (auto const& dir_entry : std::filesystem::recursive_directory_iterator{path}) 
	docs_gen::Parse(dir_entry.path().c_str(), out_path.c_str());
    }
    
    stream.close();
  }
  else std::cout << "Unable to open file";

  return 0;
}
